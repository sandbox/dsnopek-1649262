jQuery(document).ready(function() {
  var conn = io.connect('http://' + Drupal.settings.amqp_nodejs_host + ':' + Drupal.settings.amqp_nodejs_port);

  for (var i = Drupal.settings.amqp_nodejs_routing_keys.length - 1; i >= 0; i--) {
    conn.send(JSON.stringify({ key: Drupal.settings.amqp_nodejs_routing_keys[i] }));
  }

  conn.on('message', function(message) {
    $('body').trigger('notification', [message]);
  });
});