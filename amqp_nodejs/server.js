//the following settings must be defined
//this file should be moved out of the apache directory tree

var settings = {
  amqp_host: '',
  amqp_port: 5672,
  amqp_user: '',
  amqp_password: '',
  amqp_exchange: '',
  amqp_pattern: '',
  amqp_node_port: 8080
};

var util = require('util'),
    http = require('http'),
    url = require('url'),
    fs = require('fs'),
    io = require('socket.io'),
    context = require('rabbit.js').createContext(util.format('amqp://%s:%s@%s:%d', settings.amqp_user, settings.amqp_password, settings.amqp_host, settings.amqp_port));

var httpserver = http.createServer();

var socketioserver = io.listen(httpserver);

var createAMQPConnection = function(routing_key, connection) {
  var sub = context.socket('SUB');

  sub.setEncoding('utf8');
  sub.on('data', function(msg) {
    connection.send(msg);
  });

  sub.connect({ exchange: settings.amqp_exchange, pattern: settings.amqp_pattern + routing_key, routing: 'topic' });

  return sub;
};

socketioserver.sockets.on('connection', function(connection) {
  var subs = [];

  connection.on('message', function(msg) {
    msg = JSON.parse(msg);
    if(typeof msg.key !== 'undefined' && typeof subs[msg.key] === 'undefined') {
      subs.push(createAMQPConnection(msg.key, connection));
    }
  });

  connection.on('disconnect', function() {
    //destroy all AMQP connections when client disconnects
    for (var i = subs.length - 1; i >= 0; i--) {
      subs[i].destroy();
    }
  });
});

httpserver.listen(settings.amqp_node_port, '0.0.0.0');