<?php

class AMQDrupalQueue implements DrupalQueueInterface {
  /**
   * The name of the queue this instance is working with.
   *
   * @var string
   */
  protected $name;

  public function __construct($name) {
    $this->name = $name;
  }

  public function createItem($data) {
    // force the creation of the queue
    return amqp_publish('', $this->name, serialize($data));
  }

  public function numberOfItems() {
    // TODO: is there a way to get this?
    return 1;
  }

  public function claimItem($lease_time = 3600) {
    // TODO: we need a clever way to implement the lease time... We might
    // be able to do something with register_shutdown_function() to simply
    // nack() all unacked messages?

    $queue = AMQPInstance::get_queue($this->name);
    if ($message = $queue->get(AMQP_NOPARAM)) {
      $body = $message->getBody();
      if (!empty($body)) {
        $item = (object)array(
          'item_id' => $message->getDeliveryTag(),
          'data' => unserialize($body),
        );
        return $item;
      }
    }

    return FALSE;
  }

  public function deleteItem($item) {
    // TODO: handle exceptions
    $queue = AMQPInstance::get_queue($this->name);
    $queue->ack($item->item_id);
  }

  public function releaseItem($item) {
    // TODO: handle exceptions
    $queue = AMQPInstance::get_queue($this->name);
    $queue->nack($item->item_id);
  }

  public function createQueue() {
    // created on demand
    AMQPInstance::get_queue($this->name);
  }

  public function deleteQueue() {
    // TODO: handle exceptions
    AMQPInstance::get_queue($this->name)->delete();
  }
}

