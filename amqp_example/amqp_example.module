<?php

/**
 * Implementation of hook_menu().
 */
function amqp_example_menu() {
  $items = array();
  $items['amqp_example'] = array(
    'title' => t('AMQP Example'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('amqp_example_form'),
    'access arguments' => array('access content'),
  );
  return $items;
}

/**
 * Implementation of hook_amqp_queues().
 */
function amqp_example_amqp_queues() {
  // customize the options on our queue a little bit
  return array(
    'amqp_example' => array(
      // you only need to specific what is different from the defaults!
      //'flags' => AMQP_DURABLE,

      // normally we don't bind to an exchange, but rely on the default
      // exchange. Here we declaring our own new exchange.
      'exchange' => 'amqp_example',

      // By default, it makes the 'routing_key' the same as the name of
      // the queue. But you could change that here if you wanted.
      //'routing_key' => 'amqp_example',
    ),
  );
}

/**
 * Implementation of hook_amqp_exchanges().
 */
function amqp_example_amqp_exchanges() {
  return array(
    'amqp_example' => array(
      // by default, this is a direct exchange, will make this one 
      // 'fanout' just to be interesting...
      'type' => AMQP_EX_TYPE_FANOUT,
    ),
  );
}

/**
 * Implementation of hook_cron_queue_info().
 *
 * This is what Drupal 7 and the drupal_queue module for Drupal 6 
 * use to declare queue consumers!
 */
function amqp_example_cron_queue_info() {
  return array(
    // for the amqp_example queue
    'amqp_example' => array(
      'worker callback' => 'amqp_example_consumer',
      'time'            => 60,
    ),
  );
}

function amqp_example_consumer($message) {
  // just write the message to the Drupal log
  watchdog('amqp_example', 'Received message: ' . $message);
}

function amqp_example_form(&$form_state) {
  $form = array();

  if (isset($form_state['messages'])) {
    $messages = $form_state['messages'];
    if (count($messages) > 0) {
      $output = theme('item_list', $messages, t('Messages'));
    }
    else {
      $output = '<em>Nothing on the queue!</em>';
    }

    $form['info'] = array(
      '#type' => 'markup',
      '#value' => $output,
    );
  }

  $form['buttons'] = array(
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>',
  );
  $form['buttons']['publish'] = array(
    '#type' => 'submit',
    '#value' => t('Test publish'),
    '#submit' => array('_amqp_example_publish'),
  );
  $form['buttons']['consume'] = array(
    '#type' => 'submit',
    '#value' => t('Test consume'),
    '#submit' => array('_amqp_example_consume'),
  );


  return $form;
}

function _amqp_example_publish($form, &$form_state) {
  $queue = DrupalQueue::get('amqp_example');
  $queue->createQueue();
  if ($queue->createItem('Hey, what up? From Drupal! Random string: ' . user_password(10))) {
    drupal_set_message('Message sent.');
  }
  else {
    drupal_set_message('Unable to send message', 'error');
  }
}

function _amqp_example_consume($form, &$form_state) {
  // rebuild the form with our $form_state
  $form_state['rebuild'] = TRUE;
  $form_state['messages'] = array();

  // get messages
  $queue = DrupalQueue::get('amqp_example');
  $queue->createQueue();
  while ($message = $queue->claimItem()) {
    $form_state['messages'][] = $message->data;
    $queue->deleteItem($message);
  }
}

