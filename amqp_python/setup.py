from setuptools import setup, find_packages

setup(
    name="MQ Bots",
    version=0.1,
    description="AMQP MessageQueue Consumer Bots",
    author="LodgeNet Engineering",
    packages=find_packages(),
    namespace_packages = ['mq_bots'],
    install_requires = """
        PyYAML
        pika
        python-rest-client
        """,
    )

