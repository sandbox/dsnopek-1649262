import sys

import pika


class MQBot:

    def __init__(self, **configs):

        self.bindings = configs.pop('bindings', [{'exchange_name': 'something', 'routing_keys': ['binding_key']}])
        self.durable = configs.pop('durable', False)
        self.no_ack = configs.pop('no_ack', False)
        self.prefetch_count = configs.pop('prefetch_count', 1)
        self.queue_name = configs.pop('queue_name', 'queue_name')
        self.type = configs.pop('type', 'topic')

        for leftover_config_key in configs:
            setattr(self, leftover_config_key, configs[leftover_config_key])

        self.messages = self.success = 0
        self.pretty_exchanges = []
        self.pretty_routing_keys = []

        self.channel = self.__bind_to_channel()
        print "%s Ready for messages" % self.__class__.__name__
        padding = '  # '
        width = 15
        print padding + "Queue: ".rjust(width) + self.queue_name
        print padding + "Exchanges: ".rjust(width) + ', '.join(self.pretty_exchanges)
        print padding + "Routing Keys: ".rjust(width) + ';  '.join(self.pretty_routing_keys)

        try:
            self.channel.start_consuming()
        except KeyboardInterrupt:
            print "\n\t%s processed %s messages: %s succeeded, %s failed" % \
                (self.__class__.__name__, self.messages, self.success, self.messages - self.success)
            self.channel.stop_consuming()
            sys.exit()

    def __bind_to_channel(self):

            connection = pika.BlockingConnection(pika.ConnectionParameters(host='mque.ct.lodgenet.com'))
            channel = connection.channel()

            channel.queue_declare(queue=self.queue_name, durable=self.durable)
            for exchange in self.bindings:
                channel.exchange_declare(exchange=exchange['exchange_name'], type=exchange.get('type', self.type))
                self.pretty_exchanges.append(exchange['exchange_name'])
                for routing_key in exchange['routing_keys']:
                    channel.queue_bind(exchange=exchange['exchange_name'], queue=self.queue_name, routing_key=routing_key)
                    self.pretty_routing_keys.append(exchange['exchange_name'] + ': ' + routing_key)

            channel.basic_qos(prefetch_count=self.prefetch_count)
            channel.basic_consume(self.callback, queue=self.queue_name, no_ack=self.no_ack)

            return channel

    def callback(self, ch, method, properties, body):
        print "implement this in the subclass, silly"


if __name__ == '__main__':
    sys.exit("Do not run the generic MQ Bot, it doesn't do anything")
