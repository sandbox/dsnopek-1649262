#!/usr/bin/env python
import json
import yaml

import restful_lib

from mq_bots.mq_bot import MQBot


class RestBot(MQBot):

    def callback(self, ch, method, properties, body):
        print "Hit with '%s'" % body

        connection = restful_lib.Connection(self.server_uri)
        headers = {}
        headers['Content-Type'] = 'application/json'
        headers['Accept'] = 'application/json'

        result = connection.request('messages', method='post', headers=headers, body=json.dumps({'message': body, 'queue': self.queue_name}))

        if result['headers']['status'] == '200':
            ch.basic_ack(delivery_tag=method.delivery_tag)
            self.success += 1
            print "REST server said back to me: '%s' " % result['body']
        self.messages += 1


if __name__ == '__main__':
    #TODO: opt parse for yml path
    config = yaml.load(open('restbot.yml'))
    foo = RestBot(**config)
