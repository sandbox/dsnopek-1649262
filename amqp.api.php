<?php
/**
 * @file
 * Hooks provided by AMQP.
 */

/**
 * Hook for registering queues.
 * 
 * Each entry represents an exchange that will be created on demand via
 * AMQPInstance::get_queue() or DrupalQueue::get('name')->createQueue().
 *
 * Registering queues is entirely optional. If a queue is not registered,
 * the default info will simply be used.
 *
 * @return
 *   An array of queue info arrays. Each info array has a key corresponding
 *   to the name of the queue being defined.  The array value is an
 *   associative array that may contain the following key-value pairs:
 *   - "flags": A bitmask of flags. May be: AMQP_PASSIVE, AMQP_DURABLE.
 *      By default it's: AMQP_DURABLE. Use AMQP_PASSIVE if the queue
 *      is supposed to be created by another process, otherwise it will
 *      be created if it doesn't already exist.
 *   - "exchange": The name of the exchange this queue should be bound to.
 *     This must be declared in hook_amqp_exachanges(). If you don't
 *     specific any exachange, it will only be reachable through the default
 *     exchange.
 *   - "routing_key": The routing key to bind to on the exchange. The meaning
 *     of this depends on the exchange type. I recommend checking out the
 *     AMQP spec or RabbitMQ documentation for explanation of this value.
 *   - "arguments": Array of arguments.
 *
 * @see amqp_get_queue()
 * @see amqp_queue_defaults()
 * @see hook_amqp_exchanges()
 */
function hook_amqp_queues() {
  return array(
    'new_queue' => array(
      'flags'       => AMQP_DURABLE,
      'exchange'    => 'new_exchange',
      'routing_key' => 'routing_key',
      'arguments'   => array(),
    ),
  );
}

/**
 * Hook for altering the data from hook_amqp_queues().
 *
 * @param $items
 *   The array of all the registered queues.
 *
 * @see hook_amqp_queues()
 */
function hook_amqp_queues_alter(&$items) {
  if (isset($items['new_queue'])) {
    $items['new_queue']['flags'] = AMQP_PASSIVE | AMQP_DURABLE;
  }
}

/**
 * Hook for registering exchanges.
 *
 * Each entry represents an exchange that will be created on demand via when
 * trying using amqp_pubish(), AMPQ::get_exchange() or if you manipulate a
 * queue that uses this exchange via DrupalQueue::get().
 *
 * Unlike quesue, new exchanges must be declared explicitly, defaults will
 * not be used if it's undeclared.
 *
 * @return
 *   An array of exchange info arrays. Each info array has a key corresponding
 *   to the name of the exchange being defined.  The array value is an
 *   associative array that may contain the following key-value pairs:
 *   - "flags": A bitmask of flags. May be: AMQP_PASSIVE, AMQP_DURABLE.
 *      By default it's: AMQP_DURABLE. Use AMQP_PASSIVE if the exchange
 *      is supposed to be created by another process, otherwise it will
 *      be created if it doesn't already exist.
 *   - "type": The type of the exchange. Can be any of: AMQP_EX_TYPE_DIRECT
 *     (default), AMQP_EX_TYPE_FANOUT, AMQP_EX_TYPE_HEADER, AMQP_EX_TYPE_TOPIC.
 *   - "arguments": Array of arguments.
 *
 * @see amqp_get_exchange()
 * @see amqp_queue_defaults()
 */
function hook_amqp_exchanges() {
  return array(
    'new_exchange' => array(
      'flags'     => AMQP_PASSIVE,
      'type'      => AMQP_EX_TYPE_DIRECT,
      'arguments' => array(),
    ),
  );
}

/**
 * Hook for altering the data from hook_amqp_exchanges().
 *
 * @param $items
 *   The array of all the registered exchanges.
 *
 * @see hook_amqp_exchanges()
 */
function hook_amqp_exchanges_alter(&$items) {
  if (isset($items['new_exchange'])) {
    $items['new_exchange']['type'] = AMQP_EX_TYPE_FANOUT;
  }
}


/**
 * Hook for altering a message before publishing.
 *
 * @param $message
 *   The message, passed by reference.
 * @param $exchange_name
 *   The name of the exchange.
 * @param $routing_key
 *   The routing key for the message.
 */
function hook_amqp_message_alter(&$message, $exchange_name, $routing_key) {
  if ($exchange_name == 'exchange_name' && $routing_key == 'routing.key') {
  	// We can alter the message before it is published.
  	$message .= " - altered message.";
  }
}

